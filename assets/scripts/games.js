const xNumber = Math.floor(Math.random() * 20) + 1;

const inputEnter = document.querySelector('.inputEnter');
const inputGuess = document.querySelector('.inputGuess');
const pJ = document.querySelector('.pJ');
const pR1 = document.querySelector('.pR1');
const pR2 = document.querySelector('.pR2');
var arrayGuess = 1;

function startGuess() {
  var playerGuess = Number(inputGuess.value);

  if(arrayGuess === 1) {
    pR1.textContent = 'History: ';
  }
  pR1.textContent += playerGuess + ' ' + '/';

  if(playerGuess === xNumber) {
    pJ.textContent = 'You WIN!!!!';
    pR2.textContent = '';
    setGameOver();

  } else if(arrayGuess === 5) {
    pJ.textContent = 'You LOSE';
    pR2.textContent = '';
    setGameOver();

  } else {
    pJ.textContent = 'Wrong!';

    if(playerGuess < xNumber) {
      pR2.textContent = 'Wrong its too LOW';

    } else if(playerGuess >xNumber) {
      pR2.textContent = 'Wrong its too HIGH!';
    }
  }
  arrayGuess++;
  inputGuess.value = '';
  inputGuess.focus();
}

inputEnter.addEventListener('click', startGuess);
function setGameOver() {
  inputGuess.disabled = true;
  inputEnter.disabled = true;
}